﻿using NUnit.Framework;
using Server.Commands;
using Server.Generators;
using Server.Interfaces;
using Server.IoC;
using Server.Models;
using Server.Models.SpaceObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerTests.Generators
{
    internal class AdapterGeneratorTest
    {
        [Test(Description = "Тест. AdapterGenerator - GetProperty")]
        public void AdapterGenerator_GetProperty_Success()
        {
            // Arrange
            var position = new Vector(3, 4);
            var spaceShip = new SpaceShip() { Position = position };
            IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "Space.Operations.IMovable:Position.get", (Func<object[], object>)((args) => { return ((ISpaceObject)args[0]).Position; })).Execute();

            //Act
            var movableAdapter = AdapterGenerator.Generate<IMovable>(spaceShip);

            //Assert
            Assert.AreEqual(spaceShip.Position, movableAdapter.Position);
        }

        [Test(Description = "Тест. AdapterGenerator - SetProperty")]
        public void AdapterGenerator_SetProperty_Success()
        {
            // Arrange
            var position = new Vector(3, 4);
            var newPosition = new Vector(6, 7);
            var spaceShip = new SpaceShip() { Position = position };
            IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "Space.Operations.IMovable:Position.get", (Func<object[], object>)((args) => { return ((ISpaceObject)args[0]).Position; })).Execute();
            IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "Space.Operations.IMovable:Position.set", (Func<object[], object>)((args) => { ((ISpaceObject)args[0]).Position = (Vector)args[1]; return ((ISpaceObject)args[0]).Position; })).Execute();

            //Act
            var movableAdapter = AdapterGenerator.Generate<IMovable>(spaceShip);
            movableAdapter.Position = newPosition;

            //Assert
            Assert.AreEqual(newPosition, movableAdapter.Position);
        }

        [Test(Description = "Тест. AdapterGenerator - ExternalVoidMethod (IMovable.Finish)")]
        public void AdapterGenerator_ExternalVoidMethod_Success()
        {
            // Arrange
            var spaceShip = new SpaceShip();
            IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "Space.Operations.IMovable:Finish", (Func<object[], object>)((args) => { Console.WriteLine("Тест!"); return null; })).Execute();

            //Act
            var movableAdapter = AdapterGenerator.Generate<IMovable>(spaceShip);

            //Assert
            Assert.DoesNotThrow(() => movableAdapter.Finish());
        }
    }
}
