﻿using Moq;
using NUnit.Framework;
using Server.Commands;
using Server.Interfaces;
using Server.Models;
using System;

namespace ServerTests.Models.rotable
{
    internal class RotateCommandTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test(Description = "Успешный тест поворота.")]
        [TestCase(0, 3, 3)]
        [TestCase(1, 9, 2)]
        [TestCase(-1, 9, 0)]

        public void RotateCommand_Success(int currentDirectionValue, int angularVelocityValue, int expectedDirectionValue)
        {
            // Arrange
            var direction = new Direction(currentDirectionValue);
            var expectedDirection= new Direction(expectedDirectionValue);

            var rotable = new Mock<IRotable>();
            rotable.SetupProperty(p => p.Direction);
            rotable.Object.Direction = direction;

            rotable.Setup(x => x.AngularVelocity)
                .Returns(angularVelocityValue);

            var rotateCommand = new RotateCommand(rotable.Object);

            //Act
            rotateCommand.Execute();

            // Assert
            Assert.AreEqual(expectedDirection, rotable.Object.Direction);
        }

        [Test(Description = "GetDirection exception")]
        public void RotateCommand_NoDirection_Fail()
        {
            //Arrange
            var rotable = new Mock<IRotable>();
            rotable.Setup(x => x.Direction)
                .Throws(new Exception());

            var rotateCommand = new RotateCommand(rotable.Object);

            //Act
            //Assert
            Assert.Throws<Exception>(() => rotateCommand.Execute());
        }

        [Test(Description = "GetAngularVelocity exception")]
        public void RotateCommand_NoAngularVelocity_Fail()
        {
            //Arrange
            var direction = new Direction(1);

            var rotable = new Mock<IRotable>();
            rotable.Setup(x => x.Direction)
                .Returns(direction);

            rotable.Setup(x => x.AngularVelocity)
                .Throws(new Exception());

            var rotateCommand = new RotateCommand(rotable.Object);

            //Act
            //Assert
            Assert.Throws<Exception>(() => rotateCommand.Execute());
        }

        [Test(Description = "SetDirection exception")]
        public void RotateCommand_SetDirection_Fail()
        {
            // Arrange
            var direction = new Direction(1);
            var expectedPosition = new Direction(3);

            var rotable = new Mock<IRotable>();
            rotable.Setup(x => x.Direction)
                .Returns(direction);

            rotable.Setup(x => x.AngularVelocity)
                .Returns(2);

            rotable.SetupSet(p => p.Direction = It.IsAny<Direction>()).Throws(new Exception());

            var rotateCommand = new RotateCommand(rotable.Object);

            //Act
            // Assert
            Assert.Throws<Exception>(() => rotateCommand.Execute());
        }
    }
}
