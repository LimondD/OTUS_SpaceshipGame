﻿using NUnit.Framework;
using Server.Commands;
using Server.Commands.System;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ServerTests.Commands.System
{
    internal class RunCommandTests
    {
        [Test(Description = "Проверяем что поток запущен")]
        public void RunCommand_TaskRun_Success()
        {
            //Arrange
            AutoResetEvent waitHandler = new AutoResetEvent(false);
            var startCommand = new RunCommand(new List<ICommand>());

            //Act
            startCommand.Execute();
            waitHandler.WaitOne(1000);

            //Assert
            Assert.AreEqual(TaskStatus.Running, startCommand.CurrentTask.Status);
        }

        [Test(Description = "Проверяем HardStop")]
        public void RunCommand_HardStop_Success()
        {
            //Arrange
            AutoResetEvent waitHandler = new AutoResetEvent(false);
            var startCommand = new RunCommand(new List<ICommand>()
            {
                new LogCommand(new Exception())
            });
            startCommand.Queue.Enqueue(new HardStopCommand(startCommand));
            startCommand.Queue.Enqueue(new LogCommand(new Exception()));
            

            //Act
            startCommand.Execute();
            waitHandler.WaitOne(2000);

            //Assert
            Assert.AreEqual(1, startCommand.Queue.Count);
            Assert.AreEqual(TaskStatus.RanToCompletion, startCommand.CurrentTask.Status);
        }

        [Test(Description = "Проверяем SoftStop")]
        public void RunCommand_SoftStop_Success()
        {
            //Arrange
            AutoResetEvent waitHandler = new AutoResetEvent(false);
            var startCommand = new RunCommand(new List<ICommand>()
            {
                new LogCommand(new Exception())
            });
            startCommand.Queue.Enqueue(new SoftStopCommand(startCommand));
            startCommand.Queue.Enqueue(new LogCommand(new Exception()));


            //Act
            startCommand.Execute();
            waitHandler.WaitOne(2000);

            //Assert
            Assert.AreEqual(0, startCommand.Queue.Count);
            Assert.AreEqual(TaskStatus.RanToCompletion, startCommand.CurrentTask.Status);
        }
    }
}
