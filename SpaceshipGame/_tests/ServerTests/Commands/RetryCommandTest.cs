﻿using NSubstitute;
using NUnit.Framework;
using Server.Commands;
using System.Collections.Generic;

namespace ServerTests.Commands
{
    internal class RetryCommandTest
    {
        [Test(Description = "RetryCommand. Успешный вызов команды")]
        public void RetryCommand_Success()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var command = Substitute.For<ICommand>();
            var retryCommand = new RetryCommand(queue, command);

            //Act
            retryCommand.Execute();

            //Assert
            Assert.That(queue.Count, Is.EqualTo(1));
        }
    }
}
