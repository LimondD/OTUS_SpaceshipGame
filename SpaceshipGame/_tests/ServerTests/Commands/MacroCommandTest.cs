﻿using NUnit.Framework;
using Server.Commands;
using Server.Commands.Macro;
using System;
using System.Collections.Generic;

namespace ServerTests.Commands
{
    internal class MacroCommandTest
    {
        [Test(Description = "MacroCommand. Успешный вызов команды при пустом списке команд.")]
        public void MacroCommand_EmptyCommandList_Success()
        {
            //Arrange
            var commands = new List<ICommand>();
            var macroCommand = new MacroCommand(commands);

            //Act
            //Assert
            Assert.DoesNotThrow(() => macroCommand.Execute());
        }

        [Test(Description = "MacroCommand. Успешный вызов команды при НЕ пустом списке команд.")]
        public void MacroCommand_FilledCommandList_Success()
        {
            //Arrange
            var commands = new List<ICommand>()
            {
                new LogCommand(new Exception())
            };
            var macroCommand = new MacroCommand(commands);

            //Act
            //Assert
            Assert.DoesNotThrow(() => macroCommand.Execute());
        }

        [Test(Description = "MacroCommand. Неуспешный вызов.")]
        public void MacroCommand_Failed()
        {
            //Arrange
            var macroCommand = new MacroCommand(null);

            //Act
            //Assert
            Assert.Throws<Exception>(() => macroCommand.Execute());
        }
    }
}
