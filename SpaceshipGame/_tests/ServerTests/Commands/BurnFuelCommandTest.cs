﻿using NSubstitute;
using NUnit.Framework;
using Server.Commands;
using Server.Interfaces;
using System;

namespace ServerTests.Commands
{
    internal class BurnFuelCommandTest
    {
        [Test(Description = "CheckFuelComamnd. Успешный вызов команды")]
        [TestCase((uint)100, (uint)1, (uint)99)]
        [TestCase((uint)1, (uint)1, (uint)0)]
        [TestCase((uint)1, (uint)0, (uint)1)]
        [TestCase((uint)0, (uint)0, (uint)0)]

        public void BurnFuelCommand_Success(uint current, uint burnRate, uint expected)
        {
            //Arrange
            var fuelable = Substitute.For<IFuelable>();
            fuelable.CurrentFuel = current;
            fuelable.BurnRate = burnRate;
            var burnFuelCommand = new BurnFuelCommand(fuelable);

            //Act
            burnFuelCommand.Execute();

            //Assert
            Assert.AreEqual(expected, fuelable.CurrentFuel);
        }

        [Test(Description = "CheckFuelComamnd. Ошибка выполнения команды")]
        [TestCase((uint)1, (uint)2)]
        [TestCase((uint)0, (uint)1)]
        [TestCase((uint)10, (uint)100)]

        public void BurnFuelCommand_SetZeroCurrrent(uint current, uint burnRate)
        {
            //Arrange
            var fuelable = Substitute.For<IFuelable>();
            fuelable.CurrentFuel = current;
            fuelable.BurnRate = burnRate;
            var burnFuelCommand = new BurnFuelCommand(fuelable);

            //Act
            burnFuelCommand.Execute();
            //Assert
            Assert.AreEqual(0, fuelable.CurrentFuel);
        }
    }
}
