﻿using Moq;
using NUnit.Framework;
using Server.Commands;
using Server.Interfaces;
using Server.Models;
using System;

namespace ServerTests.Commands
{
    internal class ChangeVelocityCommandTest
    {
        [Test(Description = "ChangeVelocityCommand. Успешный вызов")]
        [TestCase(1, 1, 2, 0, 1, 1, -1, 1)]
        [TestCase(1, 1, 10, 0, 1, 1, -1, 1)]
        public void ChangeVelocityCommand_Success(int currentPositionX, int currentPositionY, int angularVelocity, int currentDirection, int currentVelocityX, int currentVelocityY, int expectedVelocityX, int expectedVelocityY)
        {
            //Arrange
            var spaceShip = new Mock<ISpaceObject>();
            spaceShip.Setup(x => x.Position)
                .Returns(new Vector(currentPositionX, currentPositionY));

            spaceShip.Setup(x => x.AngularVelocity)
                .Returns(angularVelocity);

            spaceShip.Setup(x => x.Direction)
                .Returns(new Direction(currentDirection));

            spaceShip.SetupProperty(x => x.Velocity);
            spaceShip.Object.Velocity = new Vector(currentVelocityX, currentVelocityY);
            var changeVelocityCommand = new ChangeVelocityCommand(spaceShip.Object);

            //Act
            changeVelocityCommand.Execute();

            //Assert
            Assert.AreEqual(new Vector(expectedVelocityX, expectedVelocityY), spaceShip.Object.Velocity);
        }

        [Test(Description = "ChangeVelocityCommand. Отсутствует скорость поворота у объекта.")]
        public void ChangeVelocityCommand_NoAngularVelocity_Failed()
        {
            //Arrange
            var spaceShip = new Mock<ISpaceObject>();
            spaceShip.Setup(x => x.AngularVelocity)
                .Throws(new NotImplementedException());
            var changeVelocityCommand = new ChangeVelocityCommand(spaceShip.Object);

            //Act
            //Assert
            Assert.Throws<Exception>(() => changeVelocityCommand.Execute());
        }
    }
}
