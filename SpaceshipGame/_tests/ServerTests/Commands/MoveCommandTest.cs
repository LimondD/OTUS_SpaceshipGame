﻿using Moq;
using NUnit.Framework;
using Server.Commands;
using Server.Interfaces;
using Server.Models;
using System;

namespace ServerTests.Commands
{
    internal class MoveCommandTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test(Description = "Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3) движение меняет положение объекта на (5, 8)")]
        public void MoveCommand_Success()
        {
            // Arrange
            var position = new Vector(12, 5);
            var velocity = new Vector(-7, 3);
            var expectedPosition = new Vector(5, 8);

            var movable = new Mock<IMovable>();
            movable.SetupProperty(m => m.Position);
            movable.Object.Position = position;

            movable.SetupGet(x => x.Velocity)
                .Returns(velocity);

            var moveCommand = new MoveCommand(movable.Object);

            //Act
            moveCommand.Execute();

            // Assert
            Assert.AreEqual(movable.Object.Position, expectedPosition);
        }

        [Test(Description = "Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке")]
        public void MoveCommand_NoPosition_Fail()
        {
            //Arrange
            var movable = new Mock<IMovable>();
            movable.Setup(x => x.Position)
                .Throws(new Exception());

            var move = new MoveCommand(movable.Object);

            //Act
            //Assert
            Assert.Throws<Exception>(() => move.Execute());
        }

        [Test(Description = "Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке")]
        public void MoveCommand_NoVelocity_Fail()
        {
            //Arrange
            var position = new Vector(12, 5);

            var movable = new Mock<IMovable>();
            movable.Setup(x => x.Position)
                .Returns(position);

            movable.Setup(x => x.Velocity)
                .Throws(new Exception());

            var moveCommand = new MoveCommand(movable.Object);

            //Act
            //Assert
            Assert.Throws<Exception>(() => moveCommand.Execute());
        }

        [Test(Description = "Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке")]
        public void MoveCommand_SetPosition_Fail()
        {
            // Arrange
            var position = new Vector(12, 5);
            var velocity = new Vector(-7, 3);
            var expectedPosition = new Vector(5, 8);

            var movable = new Mock<IMovable>();
            movable.Setup(x => x.Position)
                .Returns(position);

            movable.Setup(x => x.Velocity)
                .Returns(velocity);

            movable.SetupSet(p => p.Position = It.IsAny<Vector>()).Throws(new Exception());

            var moveCommand = new MoveCommand(movable.Object);

            //Act
            // Assert
            Assert.Throws<Exception>(() => moveCommand.Execute());
        }
    }
}
