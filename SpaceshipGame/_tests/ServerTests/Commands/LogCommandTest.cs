﻿using NUnit.Framework;
using Server.Commands;
using System;

namespace ServerTests.Commands
{
    internal class LogCommandTest
    {
        [Test(Description = "LogCommand. Успешный вызов команды")]
        public void LogCommand_Success()
        {
            //Arrange
            var logCommand = new LogCommand(new Exception());

            //Act
            //Assert
            Assert.DoesNotThrow(() => logCommand.Execute());
        }
    }
}
