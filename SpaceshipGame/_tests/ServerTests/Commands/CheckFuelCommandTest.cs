﻿using NSubstitute;
using NUnit.Framework;
using Server.Commands;
using Server.Interfaces;
using System;

namespace ServerTests.Commands
{
    internal class CheckFuelCommandTest
    {
        [Test(Description = "CheckFuelComamnd. Успешный вызов команды")]
        [TestCase((uint)100, (uint)1)]
        [TestCase((uint)1, (uint)1)]
        [TestCase((uint)1, (uint)0)]
        [TestCase((uint)0, (uint)0)]

        public void CheckFuelComamnd_Success(uint current, uint burnRate)
        {
            //Arrange
            var fuelable = Substitute.For<IFuelable>();
            fuelable.CurrentFuel = current;
            fuelable.BurnRate = burnRate;
            var checkFuelCommand = new CheckFuelCommand(fuelable);

            //Act
            //Assert
            Assert.DoesNotThrow(() => checkFuelCommand.Execute());
        }

        [Test(Description = "CheckFuelComamnd. Ошибка выполнения команды")]
        [TestCase((uint)0, (uint)1)]
        [TestCase((uint)100, (uint)101)]
        public void CheckFuelComamnd_Fail(uint current, uint burnRate)
        {
            //Arrange
            var fuelable = Substitute.For<IFuelable>();
            fuelable.CurrentFuel = current;
            fuelable.BurnRate = burnRate;
            var checkFuelCommand = new CheckFuelCommand(fuelable);

            //Act
            //Assert
            Assert.Throws<Exception>(() => checkFuelCommand.Execute());
        }
    }
}
