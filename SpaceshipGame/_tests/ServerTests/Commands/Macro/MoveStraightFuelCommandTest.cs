﻿using Moq;
using NUnit.Framework;
using Server.Commands.Macro;
using Server.Interfaces;
using Server.Models;
using System;

namespace ServerTests.Commands.Macro
{
    internal class MoveStraightFuelCommandTest
    {
        [Test(Description = "MoveStraightFuelCommand. Успешное выполнение.")]
        [TestCase(1 ,2, 1, 1, (uint)1, (uint)10, 2, 3, (uint)9)]
        [TestCase(-1, -2, 1, 2, (uint)1, (uint)10, 0, 0, (uint)9)]
        [TestCase(1, 2, 1, 1, (uint)10, (uint)10, 2, 3, (uint)0)]
        public void MoveStraightFuelCommand_Success(int positionX, int positionY, int velocityX, int velocityY, uint burnRate, uint currentFuel, int expectedPositionX, int expectedPositionY, uint expectedCurrentFuel)
        {
            //Arragne
            var spaceShip = new Mock<ISpaceObject>();
            spaceShip.Setup(x => x.Velocity)
                .Returns(new Vector(velocityX, velocityY));

            spaceShip.SetupProperty(x => x.Position);
            spaceShip.Object.Position = new Vector(positionX, positionY);

            spaceShip.Setup(x => x.BurnRate)
                .Returns(burnRate);

            spaceShip.SetupProperty(x => x.CurrentFuel);
            spaceShip.Object.CurrentFuel = currentFuel;

            var moveStraightFuelCommand = new MoveStraightFuelCommand(spaceShip.Object);

            //Act
            moveStraightFuelCommand.Execute();

            //Assert
            Assert.AreEqual(expectedCurrentFuel, spaceShip.Object.CurrentFuel);
            Assert.AreEqual(new Vector(expectedPositionX, expectedPositionY), spaceShip.Object.Position);
        }

        [Test(Description = "MoveStraightFuelCommand. Неуспешное выполнение.")]
        [TestCase(1, 2, 1, 1, (uint)10, (uint)9, 2, 3, (uint)0)]
        [TestCase(1, 2, 1, 1, (uint)1, (uint)0, 2, 3, (uint)0)]
        public void MoveStraightFuelCommand_Failed(int positionX, int positionY, int velocityX, int velocityY, uint burnRate, uint currentFuel, int expectedPositionX, int expectedPositionY, uint expectedCurrentFuel)
        {
            //Arragne
            var spaceShip = new Mock<ISpaceObject>();
            spaceShip.Setup(x => x.Velocity)
                .Returns(new Vector(velocityX, velocityY));

            spaceShip.SetupProperty(x => x.Position);
            spaceShip.Object.Position = new Vector(positionX, positionY);

            spaceShip.Setup(x => x.BurnRate)
                .Returns(burnRate);

            spaceShip.SetupProperty(x => x.CurrentFuel);
            spaceShip.Object.CurrentFuel = currentFuel;

            var moveStraightFuelCommand = new MoveStraightFuelCommand(spaceShip.Object);

            //Act
            //Assert
            Assert.Throws<Exception>(() => moveStraightFuelCommand.Execute());
        }
    }
}
