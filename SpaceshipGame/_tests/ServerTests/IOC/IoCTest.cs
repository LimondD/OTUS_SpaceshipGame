﻿using NUnit.Framework;
using Server.Commands;
using Server.IoC;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServerTests.IOC
{
    internal class IoCTest
    {
        [Test(Description = "Тест. IoC - Int")]
        public void IoC_Int_Success()
        {
            IoC.Resolve<ICommand>("IoC.Register", "Test", (Func<object[], object>)((args) => { return 5; })).Execute();
            Assert.AreEqual(5, IoC.Resolve<int>("Test"));
        }

        [Test(Description = "Тест. IoC - String")]
        public void IoC_String_Success()
        {
            IoC.Resolve<ICommand>("IoC.Register", "Test", (Func<object[], object>)((args) => { return "abc"; })).Execute();
            Assert.AreEqual("abc", IoC.Resolve<string>("Test"));
        }

        [Test(Description = "Тест. IoC - Class")]
        public void IoC_Class_Success()
        {
            IoC.Resolve<ICommand>("IoC.Register", "Test", (Func<object[], object>)((args) => { return new LogCommand(new Exception()); })).Execute();
            Assert.IsInstanceOf<LogCommand>(IoC.Resolve<LogCommand>("Test"));
        }

        [Test(Description = "Тест. IoC - скопы")]
        public void IoC_Scope_Success()
        {
            using (var scope = IoC.Resolve<Scope>("Scope.CreateAndSetNew"))
            {
                string scopeName = scope.Name;

                IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "Test",
                    (Func<object[], object>)((args) => { return 10; }))
                .Execute();

                using (IoC.Resolve<Scope>("Scope.CreateAndSetNew", scopeName))
                {
                    IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "Test",
                        (Func<object[], object>)((args) => { return 2; }))
                        .Execute();

                    Assert.AreEqual(2, IoC.Resolve<int>("Test"));
                };

                Assert.AreEqual(10, IoC.Resolve<int>("Test"));
            }
        }

        [Test(Description = "Тест. IoC - многопоточность")]
        public void IoC_MultyThread_Success()
        {
            Exception ex = null;

            Task task1 = Task.Factory.StartNew(() =>
            {
                try
                {
                    IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "t1", (Func<object[], object>)((args) => { return 2; })).Execute();
                    Assert.AreEqual(2, IoC.Resolve<int>("t1"));
                }
                catch (Exception ex1)
                {
                    ex = ex1;
                }
            });
            Task task2 = Task.Factory.StartNew(() =>
            {
                try
                {
                    IoC.Resolve<ICommand>(IoC.REGISTER_COMMAND, "t2", (Func<object[], object>)((args) => { return 5; })).Execute();
                    Assert.AreEqual(5, IoC.Resolve<int>("t2"));
                }
                catch (Exception ex2)
                {
                    ex = ex2;
                }
            });

            Task.WaitAll(task1, task2);
            Assert.IsNull(ex);
        }
    }
}
