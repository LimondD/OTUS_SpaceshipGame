﻿using NUnit.Framework;
using Server.Commands;
using Server.Exceptions.ExceptionHandler;
using System;
using System.Collections.Generic;

namespace ServerTests.Exceptions.ExceptionHandler
{
    internal class LogQueueExceptionHandlerTest
    {
        [Test(Description = "Реализовать обработчик исключения, который ставит Команду, пишущую в лог в очередь Команд")]
        public void LogQueueExceptionHandler_Success()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var exception = new Exception();
            var command = new LogCommand(exception);
            var handler = new LogQueueExceptionHandler(queue);

            //Act
            handler.Handle(command, exception);

            //Assert
            Assert.That(queue.Count, Is.EqualTo(1));
        }
    }
}
