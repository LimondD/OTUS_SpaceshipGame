﻿using NSubstitute;
using NUnit.Framework;
using Server.Commands;
using Server.Exceptions.ExceptionHandler;
using System;
using System.Collections.Generic;

namespace ServerTests.Exceptions.ExceptionHandler
{
    internal class RetryExceptionHandlerTest
    {
        [Test(Description = "Реализовать обработчик исключения, который ставит в очередь Команду - повторитель команды, выбросившей исключение")]
        public void RetryExceptionHandle_Success()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var command = Substitute.For<ICommand>();
            var handler = new RetryExceptionHandler(queue);

            //Act
            handler.Handle(command, new Exception());

            //Assert
            Assert.That(queue.Count, Is.EqualTo(1));
        }
    }
}
