﻿using NSubstitute;
using NUnit.Framework;
using Server.Commands;
using Server.Exceptions.ExceptionHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerTests.Exceptions.ExceptionHandler
{
    internal class DoubleRetryLogExceptionHandlerTest
    {
        [Test(Description = "При первом выбросе исключения повторить команду")]
        public void RetryLogException_FirstException()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var handler = new DoubleRetryLogExceptionHandler(queue);
            var command = Substitute.For<ICommand>();

            //Act
            handler.Handle(command, new Exception());

            //Assert
            var queueCommand = queue.Dequeue();
            Assert.IsInstanceOf<RetryCommand>(queueCommand);
        }

        [Test(Description = "При повторном выбросе исключения повторить команду")]
        public void RetryLogException_SecondException()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var handler = new DoubleRetryLogExceptionHandler(queue);
            var command = Substitute.For<ICommand>();
            var retryCommand = new RetryCommand(queue, command);

            //Act
            handler.Handle(retryCommand, new Exception());

            //Assert
            var queueCommand = queue.Dequeue();
            Assert.IsInstanceOf<DoubleRetryCommand>(queueCommand);
        }

        [Test(Description = "При третьем выбросе исключения записать в лог")]
        public void RetryLogException_ThirdException()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var handler = new DoubleRetryLogExceptionHandler(queue);
            var command = Substitute.For<ICommand>();
            var retryCommand = new DoubleRetryCommand(queue, command);

            //Act
            handler.Handle(retryCommand, new Exception());

            //Assert
            var queueCommand = queue.Dequeue();
            Assert.IsInstanceOf<LogCommand>(queueCommand);
        }
    }
}
