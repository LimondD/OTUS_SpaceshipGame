﻿using NSubstitute;
using NUnit.Framework;
using Server.Commands;
using Server.Exceptions.ExceptionHandler;
using System;
using System.Collections.Generic;

namespace ServerTests.Exceptions.ExceptionHandler
{
    internal class RetryLogExceptionHandlerTest
    {
        [Test(Description = "При первом выбросе исключения повторить команду")]
        public void RetryLogException_FirstException()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var handler = new RetryLogExceptionHandler(queue);
            var command = Substitute.For<ICommand>();

            //Act
            handler.Handle(command, new Exception());

            //Assert
            var queueCommand = queue.Dequeue();
            Assert.IsInstanceOf<RetryCommand>(queueCommand);
        }

        [Test(Description = "При повторном выбросе исключения записать информацию в лог")]
        public void RetryLogException_SecondException()
        {
            //Arrange
            var queue = new Queue<ICommand>();
            var handler = new RetryLogExceptionHandler(queue);
            var command = Substitute.For<ICommand>();
            var retryCommand = new RetryCommand(queue, command);

            //Act
            handler.Handle(retryCommand, new Exception());

            //Assert
            var queueCommand = queue.Dequeue();
            Assert.IsInstanceOf<LogCommand>(queueCommand);
        }
    }
}
