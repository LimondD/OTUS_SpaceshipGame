﻿namespace Server.Commands
{
    public class RetryCommand : ICommand
    {
        private readonly Queue<ICommand> _queue;
        private readonly ICommand _command;

        public RetryCommand(Queue<ICommand> queue, ICommand command)
        {
            _queue = queue;
            _command = command;
        }

        public void Execute()
        {
            _queue.Enqueue(_command);
        }
    }
}
