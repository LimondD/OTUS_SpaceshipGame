﻿using Server.Interfaces;
using Server.Models;

namespace Server.Commands
{
    public class ChangeVelocityCommand : ICommand
    {
        private readonly ISpaceObject _spaceObject;

        public ChangeVelocityCommand(ISpaceObject spaceObject)
        {
            _spaceObject = spaceObject;
        }

        public void Execute()
        {
            try
            {
                var angularVelocity = _spaceObject.AngularVelocity;
            }
            catch
            {
                throw new Exception("Отсутствует скорость поворота у объекта!");
            }

            var direction = _spaceObject.Direction.Next(_spaceObject.AngularVelocity);
            _spaceObject.Velocity = new Vector(
               (int)Math.Round(_spaceObject.Position.X * Math.Cos(Math.PI / 4 * direction.CurrentDirection) - _spaceObject.Position.Y * Math.Sin(Math.PI / 4 * direction.CurrentDirection)),
               (int)Math.Round(_spaceObject.Position.X * Math.Sin(Math.PI / 4 * direction.CurrentDirection) - _spaceObject.Position.Y * Math.Cos(Math.PI / 4 * direction.CurrentDirection))
               );
        }
    }
}
