﻿using Server.Interfaces;

namespace Server.Commands
{
    public class BurnFuelCommand : ICommand
    {
        private IFuelable _fuelable;

        public BurnFuelCommand(IFuelable fuelable)
        {
            _fuelable = fuelable;
        }

        public void Execute()
        {
            if(_fuelable.CurrentFuel < _fuelable.BurnRate)
            {
                _fuelable.CurrentFuel = 0;
                return;
            }

            _fuelable.CurrentFuel -= _fuelable.BurnRate;
        }
    }
}
