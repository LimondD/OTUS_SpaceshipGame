﻿using Server.Interfaces;

namespace Server.Commands
{
    public class CheckFuelCommand : ICommand
    {
        private readonly IFuelable _fuelable;

        public CheckFuelCommand(IFuelable fuelable)
        {
            _fuelable = fuelable;
        }

        public void Execute()
        {
            if (_fuelable.CurrentFuel < _fuelable.BurnRate)
            {
                throw new Exception("Недостаточно топлива!");
            }
        }
    }
}
