﻿namespace Server.Commands
{
    public class DoubleRetryCommand : ICommand
    {
        private readonly Queue<ICommand> _queue;
        private readonly ICommand _command;

        public DoubleRetryCommand(Queue<ICommand> queue, ICommand command)
        {
            _queue = queue;
            _command = command;
        }

        public void Execute()
        {
            _queue.Enqueue(_command);
        }
    }
}
