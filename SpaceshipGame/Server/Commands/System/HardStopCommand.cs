﻿namespace Server.Commands.System
{
    public class HardStopCommand : ICommand
    {
        private readonly RunCommand _threadSafeRunCommand;

        public HardStopCommand(RunCommand threadSafeRunCommand)
        {
            _threadSafeRunCommand = threadSafeRunCommand;
        }

        public void Execute()
        {
            _threadSafeRunCommand.IsHardStop = true;
        }
    }
}
