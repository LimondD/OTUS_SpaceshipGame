﻿namespace Server.Commands.System
{
    public class SoftStopCommand : ICommand
    {
        private readonly RunCommand _thredSafeRunCommand;

        public SoftStopCommand(RunCommand threadSafeRunCommand)
        {
            _thredSafeRunCommand = threadSafeRunCommand;
        }
        public void Execute()
        {
            _thredSafeRunCommand.IsSoftStop = true;
        }
    }
}
