﻿using System.Collections.Concurrent;

namespace Server.Commands.System
{
    public class RunCommand : ICommand
    {
        public Task CurrentTask;
        public bool IsSoftStop;
        public bool IsHardStop;
        public ConcurrentQueue<ICommand> Queue;

        public RunCommand(List<ICommand> commands)
        {
            Queue = new ConcurrentQueue<ICommand>(commands);
            CurrentTask = new Task(() =>
            {
                while (!IsHardStop && !IsSoftStop)
                {
                    while (!IsHardStop && Queue.Any())
                    {
                        try
                        {
                            Queue.TryDequeue(out var command);
                            command.Execute();
                        }
                        catch (Exception ex)
                        {
                            // Что-то делаем с Exception
                        }
                    }
                    Thread.Sleep(1000);
                }
            });
        }

        public void Execute()
        {
            CurrentTask.Start();
        }
    }
}
