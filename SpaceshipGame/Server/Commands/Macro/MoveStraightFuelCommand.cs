﻿using Server.Interfaces;

namespace Server.Commands.Macro
{
    public class MoveStraightFuelCommand : ICommand
    {
        private readonly ISpaceObject _spaceObject;

        public MoveStraightFuelCommand(ISpaceObject spaceObject)
        {
            _spaceObject = spaceObject;
        }
        public void Execute()
        {
            var commands = new List<ICommand>()
            {
                new CheckFuelCommand(_spaceObject),
                new MoveCommand(_spaceObject),
                new BurnFuelCommand(_spaceObject)
            };

            var macroCommand = new MacroCommand(commands);
            macroCommand.Execute();
        }
    }
}
