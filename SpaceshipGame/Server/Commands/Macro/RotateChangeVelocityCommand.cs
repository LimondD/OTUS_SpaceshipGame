﻿using Server.Interfaces;

namespace Server.Commands.Macro
{
    public class RotateChangeVelocityCommand : ICommand
    {
        private readonly ISpaceObject _spaceObject;

        public RotateChangeVelocityCommand(ISpaceObject spaceObject)
        {
            _spaceObject = spaceObject;
        }

        public void Execute()
        {
            var commands = new List<ICommand>()
            {
                new RotateCommand(_spaceObject),
                new ChangeVelocityCommand(_spaceObject)
            };

            var macroCommand = new MacroCommand(commands);
            macroCommand.Execute();
        }
    }
}
