﻿namespace Server.Commands.Macro
{
    public class MacroCommand : ICommand
    {
        private readonly IEnumerable<ICommand> _commands;

        public MacroCommand(IEnumerable<ICommand> commands)
        {
            _commands = commands;
        }
        public void Execute()
        {
            try
            {
                foreach (var command in _commands)
                {
                    command.Execute();
                }
            }
            catch
            {
                throw new Exception("Ошибка выполнения макрокоманды!");
            }
        }
    }
}
