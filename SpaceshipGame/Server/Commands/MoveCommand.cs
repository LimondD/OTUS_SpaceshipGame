﻿using Server.Interfaces;

namespace Server.Commands
{
    public class MoveCommand : ICommand
    {
        private readonly IMovable _moveble;

        public MoveCommand(IMovable moveble)
        {
            _moveble = moveble;
        }

        public void Execute()
        {
            _moveble.Position += _moveble.Velocity;
        }
    }
}
