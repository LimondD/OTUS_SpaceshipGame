﻿using NLog;

namespace Server.Commands
{
    public class LogCommand : ICommand
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Exception _exception;

        public LogCommand(Exception exception)
        {
            _exception = exception;
        }
        public void Execute()
        {
            Logger.Error(_exception);
        }
    }
}
