﻿using Server.Interfaces;

namespace Server.Commands
{
    public class RotateCommand : ICommand
    {
        private readonly IRotable _rotable;

        public RotateCommand(IRotable rotable)
        {
            _rotable = rotable;
        }

        public void Execute()
        {
            _rotable.Direction = _rotable.Direction
                .Next(_rotable.AngularVelocity);
        }
    }
}
