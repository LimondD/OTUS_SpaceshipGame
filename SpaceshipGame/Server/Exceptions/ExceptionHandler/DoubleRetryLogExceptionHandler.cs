﻿using Server.Commands;

namespace Server.Exceptions.ExceptionHandler
{
    public class DoubleRetryLogExceptionHandler : IExceptionHandler
    {
        private readonly Queue<ICommand> _queue;

        public DoubleRetryLogExceptionHandler(Queue<ICommand> queue)
        {
            _queue = queue;
        }

        public void Handle(ICommand command, Exception exception)
        {
            if (command is DoubleRetryCommand)
            {
                var logCommand = new LogCommand(exception);
                _queue.Enqueue(logCommand);
                return;
            }

            if (command is RetryCommand)
            {
                var doubleRetryCommand = new DoubleRetryCommand(_queue, command);
                _queue.Enqueue(doubleRetryCommand);
                return;
            }

            var retryCommand = new RetryCommand(_queue, command);
            _queue.Enqueue(retryCommand);
        }
    }
}
