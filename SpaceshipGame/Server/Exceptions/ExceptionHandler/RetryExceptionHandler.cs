﻿using Server.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Exceptions.ExceptionHandler
{
    public class RetryExceptionHandler : IExceptionHandler
    {
        private readonly Queue<ICommand> _queue;

        public RetryExceptionHandler(Queue<ICommand> queue)
        {
            _queue = queue;
        }

        public void Handle(ICommand command, Exception exception)
        {
            var retryCommand = new RetryCommand(_queue, command);
            _queue.Enqueue(retryCommand);
        }
    }
}
