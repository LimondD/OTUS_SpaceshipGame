﻿using Server.Commands;

namespace Server.Exceptions.ExceptionHandler
{
    public class LogQueueExceptionHandler : IExceptionHandler
    {
        private readonly Queue<ICommand> _queue;

        public LogQueueExceptionHandler(Queue<ICommand> queue)
        {
            _queue = queue;
        }

        public void Handle(ICommand command, Exception exception)
        {
            _queue.Enqueue(command);
        }
    }
}
