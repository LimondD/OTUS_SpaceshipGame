﻿using Server.Commands;

namespace Server.Exceptions.ExceptionHandler
{
    public class ExceptionHandler : IExceptionHandler
    {
        private readonly Dictionary<string, Action<ICommand, Exception>> _exceptionHandlers;
        public ExceptionHandler(Dictionary<string, Action<ICommand, Exception>> exceptionHandlers)
        {
            _exceptionHandlers = exceptionHandlers;
        }
        public void Handle(ICommand command, Exception exception)
        {
            var hash = HashCode.Combine(command, exception).ToString();
            var handler = (_exceptionHandlers.ContainsKey(hash)) ? _exceptionHandlers[hash] : null;
            handler?.Invoke(command, exception);
        }
    }
}
