﻿using Server.Commands;

namespace Server.Exceptions.ExceptionHandler
{
    public class RetryLogExceptionHandler : IExceptionHandler
    {
        private readonly Queue<ICommand> _queue;

        public RetryLogExceptionHandler(Queue<ICommand> queue)
        {
            _queue = queue;
        }

        public void Handle(ICommand command, Exception exception)
        {
            if (command is RetryCommand)
            {
                var logCommand = new LogCommand(exception);
                _queue.Enqueue(logCommand);
            }
            else
            {
                var retryCommand = new RetryCommand(_queue, command);
                _queue.Enqueue(retryCommand);
            }
        }
    }
}
