﻿using Server.Commands;

namespace Server.Exceptions.ExceptionHandler
{
    public interface IExceptionHandler
    {
        void Handle(ICommand command, Exception exception);
    }
}
