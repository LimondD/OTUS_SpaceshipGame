﻿using Server.Commands;

namespace Server.IoC
{
    public static class IoC
    {
        public const string REGISTER_COMMAND = "IoC.Register";
        public static ThreadLocal<string> test;
        public static Scope CurrentScope;
        public static ThreadLocal<Scope> Scope = new ThreadLocal<Scope>();
        public static readonly IDictionary<string, Func<object[], object>> types = new Dictionary<string, Func<object[], object>>();

        static IoC()
        {
            CurrentScope = new Scope(null); //Root
            Resolve<ICommand>(REGISTER_COMMAND, "Scope.CreateAndSetNew", (Func<object[], object>)((args) =>
            {
                Scope = new ThreadLocal<Scope>(() => 
                {
                    CurrentScope = new Scope(CurrentScope);
                    return CurrentScope; 
                });
                return Scope.Value; 
            })).Execute();
        }

        public static T Resolve<T>(string key, params object[] args)
        {
            if (key == REGISTER_COMMAND)
            {
                return (T) (object)new RegisterCommand((string)args[0], args[1]);
            }
            return (T)Resolve(key, args);
        }

        private static object Resolve(string key, params object[] args)
        {
                var func = FindImpementationInScope(key, CurrentScope);
                return func.Invoke(args);
        }

        private static Func<object[], object> FindImpementationInScope(string key, Scope scope)
        {
            if (types.ContainsKey($"{key}_{scope.Name}"))
            {
                return types[$"{key}_{scope.Name}"];
            }

            return FindImpementationInScope(key, scope.Parent);
        }
    }
}
