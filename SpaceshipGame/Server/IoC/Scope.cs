﻿using Server.Commands;

namespace Server.IoC
{
    public class Scope : IDisposable
    {
        public Scope Parent;
        public Scope(Scope parentScope)
        {
            Parent = parentScope;

            Name = Guid.NewGuid().ToString();
        }

        public string Name
        {
            get;
            private set;
        }

        void IDisposable.Dispose()
        {
            IoC.CurrentScope = Parent;
        }
    }
}