﻿using Server.Commands;

namespace Server.IoC
{
    public class RegisterCommand : ICommand
    {
        private readonly string _key;
        private readonly Func<object[], object> _func;

        public RegisterCommand(string key, object func)
        {
            _key = key;
            _func = (Func<object[], object>)func;
        }
        public void Execute()
        {
            IoC.types[$"{_key}_{IoC.CurrentScope.Name}"] = _func;
        }
    }
}
