﻿namespace Server.Queue.Command
{
    public interface ICommandQueue
    {
        void Run();
    }
}
