﻿// See https://aka.ms/new-console-template for more information
using NLog;
public static class Program
{
    private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public static void Main()
    {
        try
        {
            Logger.Info("Hello world");
            Console.ReadKey();
        }
        catch (Exception ex)
        {
            Logger.Error(ex, "Goodbye cruel world");
        }
    }
}
