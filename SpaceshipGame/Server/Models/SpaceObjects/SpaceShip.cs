﻿using Server.Interfaces;
using System.Reflection;

namespace Server.Models.SpaceObjects
{
    public class SpaceShip : ISpaceObject
    {
        public int AngularVelocity { get; }

        public uint CurrentFuel { get; set; }
        public uint BurnRate { get; set; }
        public Vector Position { get; set; }

        Vector IMovable.Velocity { get; set; }

        Direction IRotable.Direction { get; set; }

        public void Finish()
        {
            throw new NotImplementedException();
        }

        public object GetProperty(string key)
        {
            Type myType = typeof(SpaceShip);
            PropertyInfo myPropInfo = myType.GetProperty(key);
            return myPropInfo.GetValue(this, null);
        }

        public void SetProperty(string key, object newValue)
        {
            Type myType = typeof(SpaceShip);
            PropertyInfo myPropInfo = myType.GetProperty(key);
            myPropInfo.SetValue(this, newValue, null);
        }
    }
}
