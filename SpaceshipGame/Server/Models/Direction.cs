﻿namespace Server.Models
{
    public class Direction
    {
        const int Number = 8;
        public Direction(int direction)
        {
            CurrentDirection = direction;
        }

        public int CurrentDirection { get; set; }

        public Direction Next(int angularVelocity)
        {
            return new Direction((CurrentDirection + angularVelocity) % Number);
        }

        public override bool Equals(object obj)
        {
            var item = obj as Direction;

            if (item == null)
            {
                return false;
            }

            return item.CurrentDirection == CurrentDirection;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CurrentDirection);
        }
    }
}
