﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Server.Generators
{
    public static class AdapterGenerator
    {
        public static T Generate<T>(params object[] constructorParams)
        {
            var typeInfo = typeof(T);
            object adapater = new object();
            var adapterCode = BuildAdapterClass(typeInfo);
            // define source code, then parse it (to the type used for compilation)
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(adapterCode.ToString());

            // define other necessary objects for compilation
            string assemblyName = Path.GetRandomFileName();

            var references = AppDomain.CurrentDomain
              .GetAssemblies()
              .Where(a => !a.IsDynamic)
              .Select(a => a.Location)
              .Where(s => !string.IsNullOrEmpty(s))
              .Where(s => !s.Contains("xunit"))
              .Select(s => MetadataReference.CreateFromFile(s))
              .ToList();

            // analyse and generate IL code from syntax tree
            CSharpCompilation compilation = CSharpCompilation.Create(
                assemblyName,
                syntaxTrees: new[] { syntaxTree },
                references: references,
                options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            using (var ms = new MemoryStream())
            {
                // write IL code into memory
                EmitResult result = compilation.Emit(ms);

                if (!result.Success)
                {
                    // handle exceptions
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    foreach (Diagnostic diagnostic in failures)
                    {
                        Console.Error.WriteLine("{0}: {1}", diagnostic.Id, diagnostic.GetMessage());
                    }
                }
                else
                {
                    // load this 'virtual' DLL so that we can use
                    ms.Seek(0, SeekOrigin.Begin);
                    Assembly assembly = Assembly.Load(ms.ToArray());

                    // create instance of the desired class and call the desired function
                    Type type = assembly.GetType(GetAdapterName(typeInfo.Name));
                    adapater = Activator.CreateInstance(type, constructorParams);
                }
            }

            return (T)adapater;
        }

        private static StringBuilder BuildAdapterClass(Type typeInfo)
        {
            var adapterClassName = GetAdapterName(typeInfo.Name);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("using System;");
            sb.AppendLine("using Server.IoC;");
            sb.AppendLine("using Server.Models;");
            sb.AppendLine("using Server.Commands;");
            sb.AppendLine($"using {typeInfo.Namespace};");

            sb.AppendLine(Environment.NewLine);

            sb.AppendLine($"public class {adapterClassName} : {typeInfo.Name}");
            sb.AppendLine("{");
            sb.AppendLine("ISpaceObject obj;");
            sb.AppendLine(Environment.NewLine);
            sb.AppendLine($"public {adapterClassName}(ISpaceObject obj) {{ this.obj = obj; }}");
            sb.AppendLine(Environment.NewLine);
            foreach(var property in typeInfo.GetProperties())
            {
                sb.AppendLine($"public {property.PropertyType.Name} {property.Name}");
                sb.AppendLine("{");
                if (property.CanRead)
                {
                    sb.AppendLine($"get {{ return IoC.Resolve<{property.PropertyType.Name}>(\"Space.Operations.{typeInfo.Name}:{property.Name}.get\", obj); }}");
                }

                if (property.CanWrite)
                {
                    sb.AppendLine($"set {{ IoC.Resolve<{property.PropertyType.Name}>(\"Space.Operations.{typeInfo.Name}:{property.Name}.set\", obj, value); }}");
                }
                sb.AppendLine("}");
            }

            foreach(var method in typeInfo.GetMethods().Where(m => !m.IsSpecialName))
            {
                sb.AppendLine($"public {GetMethodReturnType(method.ReturnType.Name)} {method.Name}()");
                sb.AppendLine("{");
                sb.AppendLine($"IoC.Resolve<ICommand>(\"Space.Operations.{typeInfo.Name}:{method.Name}\");");
                sb.AppendLine("}");
            }

            sb.AppendLine("}");

            return sb;
        }
        private static string GetAdapterName(string iName)
        {
            return $"{iName.TrimStart('I')}Adapter";
        }

        private static string GetMethodReturnType(string returnTypeName)
        {
            if (returnTypeName == "Void")
            {
                return "void";
            }

            return returnTypeName;
        }
    }
}
