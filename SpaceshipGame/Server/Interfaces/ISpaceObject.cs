﻿namespace Server.Interfaces
{
    public interface ISpaceObject : IMovable, IRotable, IFuelable
    {
    }
}
