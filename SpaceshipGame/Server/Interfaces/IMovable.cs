﻿using Server.Models;

namespace Server.Interfaces
{
    public interface IMovable
    {
        Vector Position { get; set; }
        Vector Velocity { get; set; }
        void Finish();
    }
}
