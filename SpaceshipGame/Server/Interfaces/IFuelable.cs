﻿namespace Server.Interfaces
{
    public interface IFuelable
    {
        uint CurrentFuel { get; set; }
        uint BurnRate { get; set; }
    }
}
