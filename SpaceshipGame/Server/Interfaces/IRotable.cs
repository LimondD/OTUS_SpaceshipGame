﻿using Server.Models;

namespace Server.Interfaces
{
    public interface IRotable
    {
        Direction Direction { get; set; }
        int AngularVelocity { get; }
    }
}
